﻿Imports Lisi.Core

Public Class Form1
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        'Ejemplo de como llenar un objeto propiedad por propiedad
        Dim barras As Producto = New Producto()
        barras.Nombre = "B737"
        barras.Procedencia = "Francia"
        barras.Proveedor = "Lisi"
        barras.Material = Materiales.Titanio

        'Ejemplo como llenar un objeto con un constructor
        Dim ordenBarras As OrdenDeFabricacion =
            New OrdenDeFabricacion("1234", 30, barras)
        'LLenar el mismo objeto por propiedad

        'ordenBarras.Cantidad = 60
        'ordenBarras.Lote = "1234"
        'ordenBarras.Product = barras

        lblResultado.Text = "Cantida: " & ordenBarras.Cantidad
        lblResultado.Text += "Lote: " & ordenBarras.Lote

        'Ejecutar metodos publico
        ordenBarras.Product.EscribirDescripcionProductoChida()
        lblResultado.Text += ordenBarras.Product.ImprirProducto()

        Dim controller As OrdenDeFabricacionController =
            New OrdenDeFabricacionController()

        Dim barrasFabricadas As List(Of Producto) = New List(Of Producto)

        barrasFabricadas = controller.Fabricar(ordenBarras)

        Try
            MessageBox.Show("Barras fabricadas:" + barrasFabricadas.Count)
        Catch ex As Exception
            Registro.RegistrarError(ex)
        End Try

        For Each barra As Producto In barrasFabricadas
            lblResultado.Text += barra.NumeroDeSerie & " - Fue Marcado: " + barra.FueMarcado.ToString() + Environment.NewLine
        Next

        Registro.RegistrarDebug("Termino el proceso")




        System.Console.ReadLine()
    End Sub
End Class
