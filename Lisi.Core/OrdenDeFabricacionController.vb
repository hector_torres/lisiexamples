﻿Public Class OrdenDeFabricacionController

    Public Function Fabricar(orden As OrdenDeFabricacion) As List(Of Producto)
        Dim resultado As List(Of Producto) = New List(Of Producto)

        For indice As Integer = 1 To orden.Cantidad Step 1
            Dim producto As Producto = New Producto()
            producto.NumeroDeSerie = indice.ToString()

            If PuedeSerMarcado(indice) Then
                producto.FueMarcado = 1
            End If

            resultado.Add(producto)
        Next

        Return resultado
    End Function

    Private Function PuedeSerMarcado(indice As Integer) As Boolean
        Return True 'indice Mod 5 = 0
    End Function

End Class
