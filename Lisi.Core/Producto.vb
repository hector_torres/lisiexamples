﻿''' <summary>
''' Producto para ser usado en ordenes de fabricacion
''' </summary>
Public Class Producto
    Public Nombre As String
    Public Procedencia As String
    Public Proveedor As String
    Public Material As Materiales
    Private Descripcion As String
    Public NumeroDeSerie As String
    Public FueMarcado As Boolean

    ''' <summary>
    ''' Imprime la descripcion del producto
    ''' </summary>
    ''' <returns></returns>
    Public Function ImprirProducto() As String
        'EscribirDescripcionProducto()
        Return Descripcion
    End Function

    ''' <summary>
    ''' LLena la descripcion del producto 
    ''' </summary>
    Public Sub EscribirDescripcionProducto()
        Descripcion = "Producto: " & Nombre & "; Procedencia: " &
            Procedencia & ";Material: " & Material
    End Sub

    Public Sub EscribirDescripcionProductoChida()

        Dim tipoMaterial As String = ""

        'If Material = Materiales.Acero Then
        '    tipoMaterial = "Acero"
        'ElseIf Material = Materiales.Aluminio Then
        '    tipoMaterial = "Aluminio"
        'End If

        Select Case Material
            Case Materiales.Acero
                tipoMaterial = "Acero"
            Case Materiales.Aluminio
                tipoMaterial = "Aluminio"
            Case Materiales.Bronce
                tipoMaterial = "Bronce"
            Case Materiales.Titanio
                tipoMaterial = "Titanio"

        End Select

        Descripcion =
            String.Format("Producto: {0} {3}Procedencia: {1}{3}Material: {2}",
            Nombre, Procedencia, tipoMaterial, Environment.NewLine)
    End Sub
End Class
