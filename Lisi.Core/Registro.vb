﻿Public Module Registro
    Public Sub RegistrarError(ex As Exception)
        Dim hoy As Date = Date.Now
        Dim mensaje As String
        mensaje = String.Format("{0}{1}{2}", hoy, Environment.NewLine, ex.Message)
        IO.File.AppendAllText("C:\Registro.txt", mensaje)
    End Sub

    Public Sub RegistrarDebug(mensaje As String)
        Dim hoy As Date = Date.Now
        IO.File.AppendAllText("C:\Registro.txt", mensaje)
    End Sub
End Module
