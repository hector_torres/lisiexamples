﻿Imports Lisi.Core
Imports System

Module Module1

    Sub Main()
        'Ejemplo de como llenar un objeto propiedad por propiedad
        Dim barras As Producto = New Producto()
        barras.Nombre = "B737"
        barras.Procedencia = "Francia"
        barras.Proveedor = "Lisi"
        barras.Material = Materiales.Titanio

        'Ejemplo como llenar un objeto con un constructor
        Dim ordenBarras As OrdenDeFabricacion =
            New OrdenDeFabricacion("1234", 60, barras)
        'LLenar el mismo objeto por propiedad

        'ordenBarras.Cantidad = 60
        'ordenBarras.Lote = "1234"
        'ordenBarras.Product = barras

        System.Console.WriteLine("Cantida: " & ordenBarras.Cantidad)
        System.Console.WriteLine("Lote: " & ordenBarras.Lote)

        'Ejecutar metodos publico
        ordenBarras.Product.EscribirDescripcionProductoChida()
        System.Console.WriteLine(ordenBarras.Product.ImprirProducto())

        Dim controller As OrdenDeFabricacionController =
            New OrdenDeFabricacionController()

        Dim barrasFabricadas As List(Of Producto) = New List(Of Producto)

        barrasFabricadas = controller.Fabricar(ordenBarras)

        For Each barra As Producto In barrasFabricadas
            System.Console.WriteLine(barra.NumeroDeSerie)
        Next

        System.Console.ReadLine()


    End Sub

End Module
