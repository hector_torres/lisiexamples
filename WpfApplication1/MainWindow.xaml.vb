﻿Imports Lisi.Core

Class MainWindow
    Private Sub button_Click(sender As Object, e As RoutedEventArgs) Handles button.Click
        'Ejemplo de como llenar un objeto propiedad por propiedad
        Dim barras As Producto = New Producto()
        barras.Nombre = "B737"
        barras.Procedencia = "Francia"
        barras.Proveedor = "Lisi"
        barras.Material = Materiales.Titanio

        'Ejemplo como llenar un objeto con un constructor
        Dim ordenBarras As OrdenDeFabricacion =
            New OrdenDeFabricacion("1234", 60, barras)
        'LLenar el mismo objeto por propiedad

        'ordenBarras.Cantidad = 60
        'ordenBarras.Lote = "1234"
        'ordenBarras.Product = barras

        'lblResultado.Content = "Cantida: " & ordenBarras.Cantidad)
        'lblResultado.Content = "Lote: " & ordenBarras.Lote)

        lblResultado.Content = "Cantida: " & ordenBarras.Cantidad
        lblResultado.Content = "Lote: " & ordenBarras.Lote

        'Ejecutar metodos publico
        ordenBarras.Product.EscribirDescripcionProductoChida()
        lblResultado.Content += ordenBarras.Product.ImprirProducto()

        Dim controller As OrdenDeFabricacionController =
            New OrdenDeFabricacionController()

        Dim barrasFabricadas As List(Of Producto) = New List(Of Producto)

        barrasFabricadas = controller.Fabricar(ordenBarras)

        For Each barra As Producto In barrasFabricadas
            lblResultado.Content += barra.NumeroDeSerie + Environment.NewLine
        Next

        MessageBox.Show("Cantidad de Barras Creadas: " + barrasFabricadas.Count.ToString())
    End Sub
End Class
